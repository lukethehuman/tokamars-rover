# TokaMars Rover

A repository for all code written by the TokaMars team as part of the UKSEDS 2020/2021
Olympus Rover Trials.

## Using the python virtual environment

To enter the python venv for the project, type the following line from the tokamars-rover
root directory (e.g. ~/code/tokamars-rover):
```
$ source rover_venv/bin/activate
```
To test that the virtual env is working correctly, run:
```
$ python hello_colours.py
```